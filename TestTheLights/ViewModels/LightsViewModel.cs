﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTheLights.ViewModels
{
    public class LightsViewModel : ViewModelBase
    {
        public LightsViewModel()
        {
            _lights = new ObservableCollection<LightItemViewModel>();
            for (int i = 1; i <= 1000; i++)
            {
                _lights.Add(new LightItemViewModel { Number = i });
            }
        }

        public void Reset(object obj)
        {
            foreach (var item in Lights)
            {
                item.IsOn = false;
            }
            Counter = 0;
            _step = 0;
            LightsCount = 0;
        }

        int _lightsCount;
        public int LightsCount
        {
            get { return _lightsCount; }
            set
            {
                _lightsCount = value;
                OnPropertyChanged("LightsCount");
            }
        }

        ObservableCollection<LightItemViewModel> _lights;
        public ObservableCollection<LightItemViewModel> Lights
        {
            get { return _lights; }
            set
            {
                _lights = value;
                OnPropertyChanged("Lights");
            }
        }

        RelayCommand _switchCommand;
        public RelayCommand SwitchCommand { get { return _switchCommand ?? (_switchCommand = new RelayCommand(Switch)); } }

        private void Switch(object obj)
        {
            Counter++;
            foreach (var light in Lights.Where(l => l.Number % _counter == 0))
            {
                light.IsOn = !light.IsOn;
            }
            LightsCount = Lights.Where(light => light.IsOn).Count();
        }

        RelayCommand _resetCommand;
        public RelayCommand ResetCommand { get { return _resetCommand ?? (_resetCommand = new RelayCommand(Reset)); } }

        int _counter;
        public int Counter
        {
            get { return _counter; }
            set
            {
                _counter = value;
                OnPropertyChanged("Counter");
            }
        }

        int _step;
        public int Step
        {
            get { return _step; }
            set
            {
                Reset(null);
                _step = value;

                for (int i = 1; i <= value; i++)
                {
                    Switch(null);
                }
                OnPropertyChanged("Lights");
                OnPropertyChanged("Step");
            }
        }
    }
}
