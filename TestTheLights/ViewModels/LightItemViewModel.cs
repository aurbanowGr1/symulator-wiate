﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTheLights.ViewModels
{
    public class LightItemViewModel : ViewModelBase
    {
        bool _isOn;
        public bool IsOn
        {
            get
            {
                return _isOn;
            }
            set
            {
                _isOn = value;
                OnPropertyChanged("IsOn");
            }
        }

        int number;
        public int Number
        {
            get { return number; }

            set
            {
                number = value;
                OnPropertyChanged("Number");
            }
        }
    }
}
