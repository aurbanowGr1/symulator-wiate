﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace TestTheLights.ViewModels
{
    public class RelayCommand:ICommand
    {
        Predicate<object> _canExecute;
        Action<object> _execute;

        public RelayCommand(Action<object> execute, Predicate<object> canExecute) 
        {
            _execute = execute;
            _canExecute = canExecute;
        }
        public RelayCommand(Action<object> execute):this(execute, obj=>true) { }

        public bool CanExecute(object parameter)
        {
            return _canExecute(parameter);
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            _execute(parameter);
        }
    }
}
